const CleaningTaskAudit = artifacts.require("CleaningTaskAudit");

contract("CleaningTaskAudit", () => {
	let cleaningTaskAudit;
	let expectedCleaningTaskMeasurement = {
		recordAddress: "0xfc83934e4446377f20f2cf2da0fa17ed8aada2a8",
		objectId: 1,
		measurementStatus: 1,
		timestamp: 1621596267,
		temporaryPersonId: "19c398c2a9c58b395988e316a0ade87c",
		qualityScore: 25
	};


	before(async () => {
		cleaningTaskAudit = await CleaningTaskAudit.deployed();
	});

	describe("add a measurement to the smart contract and read it out", async () => {

		it("can add a measurement", async () => {
			const result = await cleaningTaskAudit.addCleaningTaskMeasurement(...Object.values(expectedCleaningTaskMeasurement));
			//console.log(result);
			//assert.i(result, 0)
		});

		// it("can retrieve the saved measurement", async () => {
		// 	let retrievedMeasurement = await cleaningTaskAudit.getCleaningTaskMeasurementsByObjectId(expectedCleaningTaskMeasurement.objectId);
		// 	assert.deepEqual(retrievedMeasurement, [Object.values(expectedCleaningTaskMeasurement).map(item => item.toString())], "Should be the expected object");
		// });
	});
});
