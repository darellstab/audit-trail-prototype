const Counter = artifacts.require("Counter");

contract("Counter", () => {
	let counter;
	let expectedCount;

	before(async () => {
		counter = await Counter.deployed();
	});

	describe("increasing and decreasing a count, read out the current value", async () => {
		before("adopt a pet using accounts[0]", async () => {
			await counter.increment();
			expectedCount = 1;
		});

		it("can read out the counter", async () => {
			const currentCounter = await counter.getCount();
			assert.equal(currentCounter, expectedCount, "The count should be at" + currentCounter);
		});
	});
});
