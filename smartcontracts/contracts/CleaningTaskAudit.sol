pragma solidity >=0.4.22 <0.8.0;


contract CleaningTaskAudit {

    struct CleaningTaskMeasurement {
        uint index;
        uint objectId;
        uint measurementStatus;
        uint timestamp;
        string temporaryPersonId;
        uint qualityScore;
    }

    mapping(address => CleaningTaskMeasurement) private cleaningTaskMeasurements;
    address[] private recordIndex;

    function addCleaningTaskMeasurement(address recordAddress, uint objectId, uint measurementStatus, uint timestamp, string memory temporaryPersonId, uint qualityScore) public returns(uint index) {
        require(!isRecord(recordAddress), "Set already contains this record.");

        CleaningTaskMeasurement memory newMeasurement = CleaningTaskMeasurement(
            {
                index: recordIndex.length - 1,
                objectId: objectId,
                measurementStatus: measurementStatus,
                timestamp: timestamp,
                temporaryPersonId: temporaryPersonId,
                qualityScore: qualityScore
            }
        );

        recordIndex.push(recordAddress);
        cleaningTaskMeasurements[recordAddress] = newMeasurement;

        return recordIndex.length-1;
    }

    function isRecord(address recordAddress)
    public
    view
    returns(bool isIndeed)
    {
        if(recordIndex.length == 0) {
            return false;
        }

        return (recordIndex[cleaningTaskMeasurements[recordAddress].index] == recordAddress);
    }

}
